const BASE_url = "https://647f229fc246f166da902475.mockapi.io/phone"
let productServ = {
    getList : () => {
        return axios({
            url : BASE_url,
            method : "GET",
        }) 

    },
    create : (phone) => {
        return axios ({
            url :BASE_url,
            method :"POST",
            data : phone,
        }) 
    },
    delete : (id) => {
        return axios ({
            url :`${BASE_url}/${id}`,
            method : "DELETE",
        })
    },
    update : (id,phone) => {
        return axios ({
            url :`${BASE_url}/${id}`,
            method : "PUT",
            data : phone
        })

    },
    getID : (id) => {
        return axios ({
            url :`${BASE_url}/${id}`,
            method : "GET",
        })

    }
}