
function renderProduct(productArr) {
  var html = "";

  productArr.forEach(function (product) {
    var productPrice = product.price;
    var productPriceFormat = new Intl.NumberFormat("vi-VN", {
      style: "currency",
      currency: "VND",
    }).format(productPrice);
    var content = `
      <div class="col">
        <div class="card">
          <div class="product_image">
            <div class="overlay">
              <div class="product_detail">
                  <p>${product.screen}</p>
                  <p>${product.backCamera}</p>
                  <p>${product.frontCamera}</p>
              </div>
            </div>
            <img
              src="${product.img}"
              class="card-img-top"
              alt="product"
            />
          </div>
          <div class="card-body">
            <h5 class="card-title">${product.name}</h5>
            <h6 class="card-title">${productPriceFormat}</h6>
            <span class="badge badge-secondary">${product.type}</span>
            <p class="card-text">${product.desc}</p>
          </div>
          <div class="card-footer">
            <button>${productPriceFormat}</button>
            <button class="btn btn-primary" id="addToCartBtn" onclick="addProductToCart(${product.id})">
              Add to cart
            </button>
          </div>
        </div>
      </div>
    `;
    html += content;
  });
  document.querySelector("#productList").innerHTML = html;
}

// Cart
function openCart() {
  const cart = document.querySelector(".cart");
  const cartOverlay = document.querySelector(".cart-overlay");
  cart.classList.toggle("active");
  cartOverlay.classList.toggle("active");
}

function closeCart() {
  const cart = document.querySelector(".cart");
  const cartOverlay = document.querySelector(".cart-overlay");
  cart.classList.remove("active");
  cartOverlay.classList.remove("active");
}

function renderCart(cartArr) {
  var html = "";
  var total = 0;
  var totalQuantity = 0;
  cartArr.forEach(function (cart) {
    var cartPrice = cart.product.price;
    var cartPriceFormat = new Intl.NumberFormat("vi-VN", {
      style: "currency",
      currency: "VND",
    }).format(cartPrice);
    var content = `
      <div class="card mb-3">
          <div class="row g-0">
            <div class="col-md-4">
              <img
                src="${cart.product.img}"
                class="img-fluid rounded-start"
              />
            </div>
            <div class="col-md-8 cartInfo">
              <div class="card-body">
                <p class="card-text">
                  <a class="text-danger" href="#!" onclick="deleteCartItem(${cart.id})">remove</a>
                </p>
                <h5 class="card-title">${cart.product.name}</h5>
                <p class="card-text">${cart.product.screen}</p>
                <p class="card-text">${cart.product.backCamera}</p>
                <p class="card-text">${cart.product.frontCamera}</p>
                <p class="card-text price">${cartPriceFormat}</p>
                <div class="quantity">
                  <button class="btn btn-outline-secondary minus" id="minus" type="button" onclick="decreaseQuantity(${cart.id})">
                    <i class="fa-solid fa-minus"></i>
                  </button>
                  <input
                    id="productQuantity${cart.id}"
                    type="text"
                    class="form-control"
                    placeholder="1"
                    aria-label="Example text with button addon"
                    aria-describedby="button-addon1"
                    value="${cart.quantity}"
                    onchange="updateCartQuantity(${cart.id}, this.value)"
                  />
                  <button class="btn btn-outline-secondary plus" id="plus" type="button" onclick="increaseQuantity(${cart.id})">
                    <i class="fa-solid fa-plus"></i>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
    `;
    html += content;
    total += cartPrice * cart.quantity;
    totalQuantity += cart.quantity * 1;
  });
  var totalFormat = new Intl.NumberFormat("vi-VN", {
    style: "currency",
    currency: "VND",
  }).format(total);
  document.querySelector(".cartProduct").innerHTML = html;
  document.querySelector("#cartQuantity").innerHTML = "Total cart quantity: " + totalQuantity;
  document.querySelector("#cartTotal").innerHTML = "Total cart value: " + totalFormat;
}
