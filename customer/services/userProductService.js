const BASE_URL = "https://647f22a0c246f166da902480.mockapi.io/product"

axios({
    url: BASE_URL,
    method: "GET"
}).then(function(res){
    renderProduct(res.data);
}).catch(function(err){
    console.log(err);
})


var productServices = {
    getListProduct: () => {
        return axios({
            url: BASE_URL,
            method: "GET"
            }).then(function(res){
                return res.data;
            }
        );
    },
    getProductById: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "GET"
            })
    }
}